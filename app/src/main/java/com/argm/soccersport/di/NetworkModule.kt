package com.argm.soccersport.di

import android.app.Application
import com.argm.soccersport.core.Constants
import com.argm.soccersport.data.network.EventTeamClient
import com.argm.soccersport.data.network.TeamClient
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    //@Singleton Mantiene un patron de una unica instancia
     @Singleton
     @Provides
     fun provideRetrofit(): Retrofit{
         return Retrofit.Builder()
             .baseUrl(Constants.URL_API)
             .addConverterFactory(GsonConverterFactory.create())
             .build()
     }

    @Singleton
    @Provides
    fun provideTeamClient(retrofit: Retrofit): TeamClient{
        return retrofit.create(TeamClient::class.java)
    }

    @Singleton
    @Provides
    fun provideEventTeamClient(retrofit: Retrofit): EventTeamClient{
        return retrofit.create(EventTeamClient::class.java)
    }
}