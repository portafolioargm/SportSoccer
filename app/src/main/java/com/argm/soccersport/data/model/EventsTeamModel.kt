package com.argm.soccersport.data.model

import androidx.room.ColumnInfo

data class EventsTeamModel(
    val results : List<EventTeamModel>
)