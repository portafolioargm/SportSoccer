package com.argm.soccersport.data.model

import androidx.room.ColumnInfo

data class EventTeamModel(
    val idTeam: String?,
    val strEvent: String?,
    val strFilename: String?,
    val intHomeScore: String?,
    val intAwayScore: String?,
    val strHomeTeam: String?,
    val strAwayTeam: String?,
    val dateEvent: String?,
    val strVenue: String?,
    val strLeague: String?,
    val strVideo: String?,
    val strThumb: String?,
    val strStatus: String?,
)