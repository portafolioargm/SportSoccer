package com.argm.soccersport.data.network

import com.argm.soccersport.core.Constants
import com.argm.soccersport.data.model.EventTeamModel
import com.argm.soccersport.data.model.TeamModel
import com.argm.soccersport.data.model.TeamsModel
import retrofit2.Response
import retrofit2.http.*

interface TeamClient {
    @GET(Constants.GET_ALL_TEAMS_FORM_LEAGUE)
    suspend fun getTeams(@Query("l") league: String?): Response<TeamsModel>
}