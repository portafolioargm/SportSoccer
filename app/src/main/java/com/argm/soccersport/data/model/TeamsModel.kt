package com.argm.soccersport.data.model

data class TeamsModel (
    val teams : List<TeamModel>
)