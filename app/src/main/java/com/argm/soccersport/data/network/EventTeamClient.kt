package com.argm.soccersport.data.network

import com.argm.soccersport.core.Constants
import com.argm.soccersport.data.model.EventTeamModel
import com.argm.soccersport.data.model.EventsTeamModel
import retrofit2.Response
import retrofit2.http.*

interface EventTeamClient {
    @GET(Constants.GET_EVENTS_FOR_TEAM)
    suspend fun getEventsTeam(@Query("id") idTeam: String): Response<EventsTeamModel>
}