package com.argm.soccersport.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class TeamModel(
    val idTeam: String?,
    val strTeam: String?,
    val strDescriptionES: String?,
    val intFormedYear: String?,
    val strTeamBadge: String?,
    val strTeamJersey: String?
):Parcelable