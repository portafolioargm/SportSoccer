package com.argm.soccersport.data.network

import com.argm.soccersport.data.model.EventTeamModel
import com.argm.soccersport.data.model.EventsTeamModel
import com.argm.soccersport.data.model.TeamModel
import com.argm.soccersport.data.model.TeamsModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Singleton

//Ojo por si hay que quitar @Singleton
@Singleton
class ApiService @Inject constructor(private val apiTeam : TeamClient, private val apiEventTeam :  EventTeamClient){
    suspend fun getTeams(league:String): TeamsModel {
        return withContext(Dispatchers.IO){
            val response = apiTeam.getTeams(league)
            (response.body() ?: emptyList<TeamModel>()) as TeamsModel
        }
    }

    suspend fun getEventsTeam(idTeam:String): EventsTeamModel {
        return withContext(Dispatchers.IO){
            val response = apiEventTeam.getEventsTeam(idTeam)
            (response.body() ?: emptyList<EventTeamModel>()) as EventsTeamModel
        }
    }
}