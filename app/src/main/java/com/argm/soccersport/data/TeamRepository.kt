package com.argm.soccersport.data

import com.argm.soccersport.data.model.TeamsModel
import com.argm.soccersport.data.network.ApiService
import com.argm.soccersport.domain.model.Team
import com.argm.soccersport.domain.model.toDomain
import javax.inject.Inject

class TeamRepository  @Inject constructor(
    private val api : ApiService,
    //private val teamTable : TeamDAO
    ){
    suspend fun getAllTeamsFromApi(league : String): TeamsModel {
        val response = api.getTeams(league)
        return response
    }
/*
    suspend fun getAllTeamsFromDB(): List<Team>{
        val response = teamTable.getTeamsDao()
        return response.map { it.toDomain() }
    }

    suspend fun insertTeams(teams : List<Team>){
        teamTable.insertAll(teams.map { it.toDatabase() })
    }

    suspend fun clearTeams(){
        teamTable.delete()
    }


 */
}