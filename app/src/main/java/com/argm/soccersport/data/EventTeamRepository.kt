package com.argm.soccersport.data

import com.argm.soccersport.data.model.EventsTeamModel
import com.argm.soccersport.domain.model.toDomain
import com.argm.soccersport.data.network.ApiService
import com.argm.soccersport.domain.model.EventTeam
import javax.inject.Inject

class EventTeamRepository @Inject constructor(
    private val api : ApiService,
    //private val eventTeamTable : EventTeamDAO
    ){
    suspend fun getAllEventTeamFromApi(idTeam : String): EventsTeamModel {
        val response = api.getEventsTeam(idTeam)
        return response
    }
/*
    suspend fun getAllEventTeamFromDB(idTeam : String): List<EventTeam>{
        val response = eventTeamTable.getEventTeamsDao(idTeam)
        return response.map { it.toDomain() }
    }

    suspend fun insertEventsTeam(eventsTeam : List<EventTeam>){
        eventTeamTable.insertAll(eventsTeam.map { it.toDatabase() })
    }

    suspend fun clearEventsTeam(idTeam : String){
        eventTeamTable.delete(idTeam)
    }


 */
}