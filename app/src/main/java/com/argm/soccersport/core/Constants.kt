package com.argm.soccersport.core
object Constants {
    private const val DOMAIN = "https://www.thesportsdb.com/api/"
    private const val API_VERSION = "v1/"
    private const val FORMAT_RETURN = "json/"
    private const val TOKEN = "2/"
    const val URL_API = "$DOMAIN$API_VERSION$FORMAT_RETURN$TOKEN"
    const val NAME_LEAGUE1 = "Spanish La Liga"
    const val NAME_LEAGUE2 = "French Ligue 1"
    const val NAME_LEAGUE3 = "English Premier League"
    const val GET_ALL_TEAMS_FORM_LEAGUE = "search_all_teams.php"
    const val GET_EVENTS_FOR_TEAM = "eventslast.php"
    const val VERSION_SQLITE = 1
    const val NAME_DB = "soccersport"

}