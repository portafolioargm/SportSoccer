package com.argm.soccersport

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class SoccerSportApp: Application() {
}