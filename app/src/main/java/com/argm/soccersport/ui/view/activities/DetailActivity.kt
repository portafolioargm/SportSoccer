package com.argm.soccersport.ui.view.activities

import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.argm.soccersport.R
import com.argm.soccersport.core.Constants
import com.argm.soccersport.data.model.TeamModel
import com.argm.soccersport.databinding.ActivityDetailBinding
import com.argm.soccersport.databinding.ActivityMainBinding
import com.argm.soccersport.ui.view.adapters.EventTeamAdapter
import com.argm.soccersport.ui.view.adapters.TeamAdapter
import com.argm.soccersport.ui.viewModel.EventTeamViewModel
import com.argm.soccersport.ui.viewModel.TeamViewModel
import com.bumptech.glide.Glide
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class DetailActivity() : AppCompatActivity() {
    private lateinit var binding: ActivityDetailBinding
    private val eventTeamViewModel: EventTeamViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val bundle = intent.extras
        val team = bundle?.getParcelable<TeamModel>("team")!!
        binding = ActivityDetailBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.rvEventTeams.layoutManager = LinearLayoutManager(this)
        binding.encabezadoTeam.txItemTeamName.text = team.strTeam
        binding.encabezadoTeam.txItemTeamFoundationYear.text = team.intFormedYear
        Glide
            .with(this)
            .load(team.strTeamBadge)
            .centerInside()
            .into(binding.encabezadoTeam.imgItemTeamBadge);
        Glide
            .with(this)
            .load(team.strTeamJersey)
            .centerInside()
            .into(binding.encabezadoTeam.imgItemTeamJersey);
        binding.txItemTeamDescription.text = team.strDescriptionES

        eventTeamViewModel.onCreate(team.idTeam!!)

        eventTeamViewModel.eventsteamModel.observe(this, Observer {
            binding.rvEventTeams.adapter = EventTeamAdapter(eventTeamViewModel.eventsteamModel.value!!)
        })
        eventTeamViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvEventTeams.isVisible = !it
        })
    }
}