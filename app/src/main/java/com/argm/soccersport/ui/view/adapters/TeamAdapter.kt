package com.argm.soccersport.ui.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.argm.soccersport.R
import com.argm.soccersport.data.model.TeamsModel

class TeamAdapter(private val teams : TeamsModel): RecyclerView.Adapter<TeamViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TeamViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return TeamViewHolder(inflater.inflate(R.layout.item_team, parent, false))
    }

    override fun onBindViewHolder(holder: TeamViewHolder, position: Int) {
        val item = teams.teams[position]
        holder.render(item)
    }

    override fun getItemCount(): Int {
        return teams.teams.size
    }

}