package com.argm.soccersport.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.soccersport.data.model.EventTeamModel
import com.argm.soccersport.data.model.EventsTeamModel
import com.argm.soccersport.data.model.TeamModel
import com.argm.soccersport.data.model.TeamsModel
import com.argm.soccersport.domain.EventTeamUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class EventTeamViewModel @Inject constructor(
    private val eventTeamUseCase : EventTeamUseCase
) : ViewModel() {
    val eventsteamModel = MutableLiveData<EventsTeamModel>()
    val isLoading =  MutableLiveData<Boolean>()

    fun onCreate(idTeam:String) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = eventTeamUseCase(idTeam)
            if (!result.results.isNullOrEmpty()) {
                eventsteamModel.postValue(result)
            }
            isLoading.postValue(false)
        }
    }
}