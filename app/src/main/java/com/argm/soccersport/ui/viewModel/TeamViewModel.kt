package com.argm.soccersport.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.argm.soccersport.core.Constants
import com.argm.soccersport.data.model.TeamModel
import com.argm.soccersport.data.model.TeamsModel
import com.argm.soccersport.domain.TeamUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject
import kotlinx.coroutines.launch

@HiltViewModel
class TeamViewModel @Inject constructor(
    private val teamUseCase : TeamUseCase
) : ViewModel() {
    val teamsModel = MutableLiveData<TeamsModel>()
    val isLoading =  MutableLiveData<Boolean>()

    fun onCreate(league:String) {
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = teamUseCase(league)
            if (!result.teams.isNullOrEmpty()) {
                teamsModel.postValue(result)
            }
            isLoading.postValue(false)
        }
    }
}