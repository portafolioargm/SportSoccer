package com.argm.soccersport.ui.view.activities

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.argm.soccersport.R
import com.argm.soccersport.core.Constants
import com.argm.soccersport.databinding.ActivityMainBinding
import com.argm.soccersport.ui.view.adapters.TeamAdapter
import com.argm.soccersport.ui.viewModel.TeamViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private val teamViewModel: TeamViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val leagues = resources.getStringArray(R.array.leagues)
        binding.rvTeams.layoutManager = LinearLayoutManager(this)
        binding.spTeams.adapter = ArrayAdapter(this, R.layout.spinner_item_custom, leagues)
        binding.spTeams.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onItemSelected(
                parent: AdapterView<*>,
                view: View,
                position: Int,
                id: Long
            ) {
                leagues[position]
                teamViewModel.onCreate(leagues[position])
            }

            override fun onNothingSelected(parent: AdapterView<*>) {
                // write code to perform some action
            }
        }
        binding.spTeams.setSelection(0)
        teamViewModel.teamsModel.observe(this, Observer {
            binding.rvTeams.adapter = TeamAdapter(teamViewModel.teamsModel.value!!)
            binding.txEquipos.text = "Equipos: " + it.teams.size
            binding.txLeague.text = "Torneo: " + leagues[binding.spTeams.selectedItemPosition]
        })
        teamViewModel.isLoading.observe(this, Observer {
            binding.progress.isVisible = it
            binding.rvTeams.isVisible = !it
        })
    }
}