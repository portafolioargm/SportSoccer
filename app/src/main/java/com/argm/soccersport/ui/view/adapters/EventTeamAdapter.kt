package com.argm.soccersport.ui.view.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.argm.soccersport.R
import com.argm.soccersport.data.model.EventsTeamModel
import com.argm.soccersport.data.model.TeamsModel

class EventTeamAdapter(private val events : EventsTeamModel): RecyclerView.Adapter<EventsTeamViewHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EventsTeamViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return EventsTeamViewHolder(inflater.inflate(R.layout.item_events, parent, false))
    }

    override fun onBindViewHolder(holder: EventsTeamViewHolder, position: Int) {
        val item = events.results[position]
        holder.render(item)
    }

    override fun getItemCount(): Int {
        return events.results.size
    }

}