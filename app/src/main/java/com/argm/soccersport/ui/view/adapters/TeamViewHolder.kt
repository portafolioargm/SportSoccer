package com.argm.soccersport.ui.view.adapters

import android.content.Intent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.argm.soccersport.R
import com.argm.soccersport.data.model.TeamModel
import com.argm.soccersport.ui.view.activities.DetailActivity
import com.bumptech.glide.Glide

class TeamViewHolder(val view: View):RecyclerView.ViewHolder(view) {
    private val layout = view.findViewById<View>(R.id.layoutItemTeam)
    private val txItemTeamName = view.findViewById<TextView>(R.id.txItemTeamName)
    private val txItemTeamFoundationYear = view.findViewById<TextView>(R.id.txItemTeamFoundationYear)
    private val imgItemTeamBadge = view.findViewById<ImageView>(R.id.imgItemTeamBadge)
    private val imgItemTeamJersey = view.findViewById<ImageView>(R.id.imgItemTeamJersey)

    fun render(team: TeamModel){
        txItemTeamName.text = team.strTeam
        txItemTeamFoundationYear.text = team.intFormedYear
        Glide
            .with(imgItemTeamBadge.context)
            .load(team.strTeamBadge)
            .centerInside()
            .into(imgItemTeamBadge);
        Glide
            .with(imgItemTeamJersey.context)
            .load(team.strTeamJersey)
            .centerInside()
            .into(imgItemTeamJersey);
        layout.setOnClickListener(View.OnClickListener {
            val detail = Intent(layout.context, DetailActivity::class.java)
            detail.putExtra("team", team)
            layout.context.startActivity(detail)
        })
    }
}