package com.argm.soccersport.ui.view.adapters

import android.content.Intent
import android.net.Uri
import android.service.autofill.TextValueSanitizer
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.argm.soccersport.R
import com.argm.soccersport.data.model.EventTeamModel
import com.argm.soccersport.data.model.EventsTeamModel
import com.argm.soccersport.data.model.TeamModel
import com.bumptech.glide.Glide

class EventsTeamViewHolder(view: View):RecyclerView.ViewHolder(view) {
    private val txItemEventDate = view.findViewById<TextView>(R.id.txItemEventDate)
    private val txItemEventStadium = view.findViewById<TextView>(R.id.txItemEventStadium)
    private val txItemEventHomeName = view.findViewById<TextView>(R.id.txItemEventNameHome)
    private val txItemEventHomeScore = view.findViewById<TextView>(R.id.txItemEventHomeScore)
    private val txItemEventAwayName = view.findViewById<TextView>(R.id.txItemEventAwayName)
    private val txItemEventAwayScore = view.findViewById<TextView>(R.id.txItemEventAwayScore)
    private val imgItemEventAnuncio = view.findViewById<ImageView>(R.id.imgItemEventAnuncio)
    private val imgItemEventVideo = view.findViewById<ImageView>(R.id.imgItemEventVideo)
    private val txItemEventLeague = view.findViewById<TextView>(R.id.txItemEventLeague)

    fun render(event: EventTeamModel){
        txItemEventDate.text = event.strStatus + "(${event.dateEvent})"
        txItemEventStadium.text = event.strVenue
        txItemEventHomeName.text = event.strHomeTeam
        txItemEventHomeScore.text = event.intHomeScore
        txItemEventAwayName.text = event.strAwayTeam
        txItemEventAwayScore.text = event.intAwayScore
        Glide
            .with(imgItemEventAnuncio.context)
            .load(event.strThumb)
            .centerInside()
            .into(imgItemEventAnuncio)
        if (event.strVideo.isNullOrEmpty()){
            imgItemEventVideo.visibility = View.GONE
            imgItemEventVideo.tag = null
        }else{
            imgItemEventVideo.visibility = View.VISIBLE
            imgItemEventVideo.setOnClickListener(View.OnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.data = Uri.parse(event.strVideo)
                imgItemEventVideo.context.startActivity(intent)
            })
        }
        txItemEventLeague.text = event.strLeague
    }
}