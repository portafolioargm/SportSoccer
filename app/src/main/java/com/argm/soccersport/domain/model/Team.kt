package com.argm.soccersport.domain.model

import com.argm.soccersport.data.model.TeamModel

data class Team (
    val idTeam: String?,
    val strTeam: String?,
    val strDescriptionES: String?,
    val intFormedYear: String?,
    val strTeamBadge: String?,
    val strTeamJersey: String?
)

fun TeamModel.toDomain() = Team(
    idTeam,
    strTeam,
    strDescriptionES,
    intFormedYear,
    strTeamBadge,
    strTeamJersey)
/*
fun TeamEntity.toDomain() = Team(
    idTeam,
    strTeam,
    strDescriptionES,
    intFormedYear,
    strTeamBadge,
    strTeamJersey
)
 */