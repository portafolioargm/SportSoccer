package com.argm.soccersport.domain;

import com.argm.soccersport.data.TeamRepository
import com.argm.soccersport.data.model.TeamsModel
import com.argm.soccersport.domain.model.Team
import javax.inject.Inject

class TeamUseCase @Inject constructor(private val repository : TeamRepository) {
    suspend operator fun invoke(league:String): TeamsModel{
        val teams = repository.getAllTeamsFromApi(league)
        return teams
        /*return if (teams.isNotEmpty()){
            repository.clearTeams()
            repository.insertTeams(teams)
            teams
        }else{
            repository.getAllTeamsFromDB()
        }*/
    }
}
