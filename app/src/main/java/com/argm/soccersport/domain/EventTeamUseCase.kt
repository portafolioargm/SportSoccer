package com.argm.soccersport.domain;

import com.argm.soccersport.data.EventTeamRepository
import com.argm.soccersport.data.model.EventsTeamModel
import com.argm.soccersport.domain.model.EventTeam
import javax.inject.Inject

class EventTeamUseCase  @Inject constructor(private var repository : EventTeamRepository){
    suspend operator fun invoke(idTeam:String): EventsTeamModel{
       val eventsTeam = repository.getAllEventTeamFromApi(idTeam)
        return eventsTeam
        /*return if (eventsTeam.isNotEmpty()){
            repository.clearEventsTeam(idTeam)
            repository.insertEventsTeam(eventsTeam)
            eventsTeam
        }else{
            repository.getAllEventTeamFromDB(idTeam)
        }*/
    }
}
