package com.argm.soccersport.domain.model

import com.argm.soccersport.data.model.EventTeamModel

data class EventTeam(
    val idTeam: String?,
    val strEvent: String?,
    val strFilename: String?,
    val intHomeScore: String?,
    val intAwayScore: String?,
    val strHomeTeam: String?,
    val strAwayTeam: String?,
    val strVenue: String?,
    val strLeague: String?,
    val strVideo: String?,
    val strThumb: String?,
    val strStatus: String?
)

/*
fun EventTeamModel.toDomain() = EventTeam(
    idTeam,
    strEvent,
    strFilename,
    intHomeScore,
    intAwayScore,
    strVenue,
    strLeague,
    strVideo,
    strThumb,
    strStatus,
)

fun EventTeamEntity.toDomain() = EventTeam(
    idTeam,
    strEvent,
    strFilename,
    intHomeScore,
    intAwayScore,
    strVenue,
    strLeague,
    strVideo,
    strThumb,
    strStatus,
)

 */